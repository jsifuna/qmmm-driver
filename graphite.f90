module graphite_m
  !! This module deals with auxiliary routines for MM graphene setups.

  implicit none
  private
  public :: find_graphite_layers

contains
  subroutine find_graphite_layers( nac, attype, ng1, bondxat, &
                                   graphite_layer_no )

    !! For each MM atom, this subroutine assigns the graphite layer to which
    !! each MM atom belongs.
    implicit none
    integer         , intent(in)    :: nac
      !! Number of MM atoms.
    character(len=4), intent(in)    :: attype(nac)
      !! MM atom types.
    integer         , intent(in)    :: ng1(nac,6)
      !! First neighbours indices for each atom in connectivity.
    integer         , intent(in)    :: bondxat(nac)
      !! Number of bonds for each MM atom.
    integer         , intent(inout) :: graphite_layer_no(nac)
      !! Graphite layer correspondence for each MM atom.

    integer :: iat, num_of_graphite_layers

    num_of_graphite_layers = 0
    do iat = 1, nac
      if ( (graphite_layer_no(iat) /= 0) .or. (attype(iat) /= 'C#') ) cycle

      num_of_graphite_layers = num_of_graphite_layers +1
      graphite_layer_no(iat) = num_of_graphite_layers
      call check_graphite_connectivity( iat, nac, attype, ng1, bondxat, &
                                        graphite_layer_no )
    enddo
  end subroutine find_graphite_layers

  recursive subroutine check_graphite_connectivity( iat, nac, attype, ng1, &
                                                    bondxat, graphite_layer_no )
    !! Assigns the graphene layer number of a given atom by recursively exploring
    !! its connectivity.
    implicit none
    integer         , intent(in)    :: iat
      !! Currrent MM atom index.
    integer         , intent(in)    :: nac
      !! Number of MM atoms.
    character(len=4), intent(in)    :: attype(nac)
      !! MM atom types.
    integer         , intent(in)    :: ng1(nac,6)
      !! First neighbours indices for each atom in connectivity.
    integer         , intent(in)    :: bondxat(nac)
      !! Number of bonds for each MM atom.
    integer         , intent(inout) :: graphite_layer_no(nac)
      !! Graphite layer correspondence for each MM atom.

    integer :: jat

    do jat = 1, bondxat(iat)
      if ( (graphite_layer_no(ng1(iat,jat)) /= 0) .or. (attype(iat) /= 'C#') )&
        cycle
      graphite_layer_no(ng1(iat,jat)) = graphite_layer_no(iat)
      call check_graphite_connectivity( ng1(iat,jat), nac, attype, ng1, &
                                        bondxat, graphite_layer_no )
    enddo
  end subroutine check_graphite_connectivity
end module graphite_m