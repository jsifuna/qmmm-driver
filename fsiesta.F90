module fsiesta

  use precision, only: dp

! Support routines for siesta-as-a-subroutine in Unix/Linux.
! The routines that handle the other side of the communication are
! in module iopipes of siesta program.
! Usage:
!   call siesta_launch( label, nnodes )
!     character(len=*),intent(in) :: label  : Name of siesta process
!                                             (prefix of its .fdf file)
!     integer,optional,intent(in) :: nnodes : Number of MPI nodes
!
!   call siesta_units( length, energy )
!     character(len=*),intent(in) :: length : Physical unit of length
!     character(len=*),intent(in) :: energy : Physical unit of energy
!
!   call siesta_forces( label, na, xa, cell, energy, fa, stress )
!     character(len=*), intent(in) :: label      : Name of siesta process
!     integer,          intent(in) :: na         : Number of atoms
!     real(dp),         intent(in) :: xa(3,na)   : Cartesian coords
!     real(dp),optional,intent(in) :: cell(3,3)  : Unit cell vectors
!     real(dp),optional,intent(out):: energy     : Total energy
!     real(dp),optional,intent(out):: fa(3,na)   : Atomic forces
!     real(dp),optional,intent(out):: stress(3,3): Stress tensor
!   call siesta_quit( label )
!     character(len=*),intent(in) :: label  : Name of one siesta process,
!                                             or 'all' to stop all procs.
! Behaviour:
! - If nnodes is not present among siesta_launch arguments, or nnodes<2,
!   a serial siesta process will be launched. Otherwise, a parallel
!   mpirun process will be launched.
! - If siesta_units is not called, length='Ang', energy='eV' are
!   used by default. If it is called more than once, the units in the
!   last call become in effect.
! - The physical units set by siesta_units are used for all the siesta
!   processes launched
! - If siesta_forces is called without a previous call to siesta_launch
!   for that label, it assumes that the siesta process has been launched
!   (and the communication pipes created) externally in the shell.
!   In this case, siesta_forces only opens its end of the pipes and begins
!   communication through them.
! - If argument cell is not present in the call to siesta_forces, or if
!   the cell has zero volume, it is assumed that the system is a molecule,
!   and a supercell is generated automatically by siesta so that the
!   different images do not overlap. In this case the stress returned
!   has no physical meaning.
! - The stress is defined as dE/d(strain)/Volume, with a positive sign
!   when the system tends to contract (negative pressure)
! - The following events result in a stopping error message:
!   - siesta_launch is called twice with the same label
!   - siesta_forces finds a communication error trough the pipes
!   - siesta_quit is called without a prior call to siesta_launch or
!     siesta_forces for that label
! - If siesta_quit is not called for a launched siesta process, that
!   process will stay listening indefinitedly to the pipe and will need
!   to be killed in the shell.
! - siesta_units may be called either before or after siesta_launch
! J.M.Soler and A.Garcia. Nov.2003

   use fdf

  implicit none

PUBLIC :: siesta_launch, siesta_units, siesta_send_coordinates, &
               siesta_receive_forces, siesta_quit, create_fifos

PRIVATE ! Nothing is declared public beyond this point

! Holds data on siesta processes and their communication pipes
  type proc
    private
    character(len=80) :: label     ! Name of process
    integer           :: iuc, iuf  ! I/O units for coords/forces commun.
  end type proc

! Global module variables
  integer, parameter :: max_procs = 100
  type(proc),   save :: p(max_procs)
  integer,      save :: np=0
  character(len=32), save :: xunit = 'Ang'
  character(len=32), save :: eunit = 'eV'
  character(len=32), save :: funit = 'eV/Ang'
  character(len=32), save :: sunit = 'eV/Ang**3'

CONTAINS

  subroutine create_fifos( label)

    use linkatoms, only : num_resonances, resonance

    implicit none
    character(len=*),  intent(in) :: label

    character(len=45) :: cpipe, fpipe
    character(len=120) :: task
    integer           :: ip, iu

    integer :: resonance_id
    logical :: lex

    do resonance_id=1,num_resonances

       ! Check that pipe does not exist already
       if (idx(trim(resonance(resonance_id)%path)//label) /= 0) &
            print*, 'siesta_launch: ERROR: process for label ', trim(label), &
            ' already launched'

       ! Create pipes
       cpipe = trim(resonance(resonance_id)%path)//trim(label)//'.coords'
       fpipe = trim(resonance(resonance_id)%path)//trim(label)//'.forces'

       call system('rm -f '//cpipe)
       call system('rm -f '//fpipe)
       task = 'mkfifo '//trim(cpipe)//&
            ' '//trim(fpipe)
       call system(task)

       ! Open this side of pipes
       call open_pipes( trim(resonance(resonance_id)%path)//label )

       ! Send wait message to coordenates pipe
       ip = idx( trim(resonance(resonance_id)%path)//label )
       iu = p(ip)%iuc
       write(iu,*) 'wait'

       if (resonance_id>1) then
          inquire(file=trim(resonance(resonance_id)%path)//'siesta',EXIST=lex)
          call system('rm -f '//trim(resonance(resonance_id)%path)//'siesta')
          call system('ln -s ../siesta '//trim(resonance(resonance_id)%path))
       endif

    enddo

  end subroutine create_fifos

  subroutine siesta_launch( label )

    use linkatoms, only : num_resonances, resonance
    use siesta_qmmm_options, only : launch_siesta_flag

    implicit none
    character(len=*),  intent(in) :: label

    character(len=120) :: task

    integer :: resonance_id

    integer :: siesta_nodes

    logical :: lex

! set processor number for the QM siesta calculation
    siesta_nodes=fdf_integer('NumberMPInodes',1)

    do resonance_id=1,num_resonances

       if (launch_siesta_flag) then
          if (resonance_id>1) then
             inquire(file=trim(resonance(resonance_id)%path)//'siesta',EXIST=lex)
             call system('rm -f '//trim(resonance(resonance_id)%path)//'siesta')
             call system('ln -s ../siesta '//trim(resonance(resonance_id)%path))
          endif
          ! Start siesta process
          if (siesta_nodes>1) then
             write(task,*) 'cd '//trim(resonance(resonance_id)%path)//&
                 ' ; mpirun -np ', siesta_nodes, ' ./siesta < ', &
                  trim(label)//'.fdf > ', trim(label)//'.out &'
          else
             write(task,*) 'cd '//trim(resonance(resonance_id)%path)//&
               ' ; ./siesta < ', &
                 trim(label)//'.fdf > ', trim(label)//'.out &'
          end if
          call system(task)
          write(*,FMT='(A,I4,A)') &
              'SIESTA has been launched on ',siesta_nodes,' procs'
          call pxfflush(6)
       else
          call system('echo 0 > '//trim(resonance(resonance_id)%path)//&
                        'start_siesta')
       endif

    enddo

  end subroutine siesta_launch

  !---------------------------------------------------

  subroutine siesta_units( length, energy )
    implicit none
    character(len=*), intent(in) :: length, energy
    xunit = length
    eunit = energy
    funit = trim(eunit)//'/'//trim(xunit)
    sunit = trim(eunit)//'/'//trim(xunit)//'**3'
  end subroutine siesta_units

  !---------------------------------------------------

  subroutine siesta_send_coordinates( label, na_qm, rqm, cell )

    use linkatoms, only : num_resonances, resonance, numlink, rlink

    implicit none
    character(len=*),   intent(in) :: label
    integer,            intent(in) :: na_qm
    real(dp),           intent(in) :: rqm(3,na_qm)
    real(dp), optional, intent(in) :: cell(3,3)
    real(dp)          :: c(3,3)

    integer           :: i, ia, ip, iu

    integer :: resonance_id

    logical           firstTime
    save              firstTime
    data              firstTime /.true./


    if (firstTime) then
      call siesta_launch( label )
      firstTime=.false.
    endif

    do resonance_id=1,num_resonances

       ! Find system index
       ip = idx( trim(resonance(resonance_id)%path)//label )
       if (ip==0) then
          call open_pipes( label )
          ip = idx( label )
          stop 'Openning pipes??'
       end if

       ! Copy unit cell
       if (present(cell)) then
          c = cell
       else
          c = 0._dp
       end if

       ! Print coords for debugging
       print'(/,2a)',         'siesta_send_coordinates: label = ', &
                    trim(resonance(resonance_id)%path)//trim(label)
       print'(3a,/,(3f12.6))','siesta_send_coordinates: cell (',trim(xunit),') =',cell
       print'(3a,/,(3f12.6))','siesta_send_coordinates: rqm (',trim(xunit),') =', rqm
       print'(3a,/,(3f12.6))','siesta_send_coordinates: rlink (',trim(xunit),') =',&
             (rlink(resonance_id,1:3,i),i=1,numlink)
       print*,'##################################################'

       ! Write coordinates to pipe
       iu = p(ip)%iuc
       write(iu,*) 'begin_coords'
       write(iu,*) trim(xunit)
       write(iu,*) trim(eunit)
       do i = 1,3
          write(iu,*) c(:,i)
       end do
       write(iu,*) na_qm+numlink
       do ia = 1,na_qm
          write(iu,*) rqm(:,ia)
       end do
       do ia = 1,numlink
          write(iu,*) rlink(resonance_id,:,ia)
       end do
       write(iu,*) 'end_coords'
       call pxfflush(iu)

    enddo

  end subroutine siesta_send_coordinates

  subroutine siesta_receive_forces( label, na_qm, energy, fa, stress )

    use linkatoms, only : num_resonances, resonance, numlink, fa_link

    implicit none

    character(len=*),   intent(in) :: label
    integer,            intent(in) :: na_qm
    real(dp), optional, intent(out):: energy
    real(dp), optional, intent(out):: fa(3,na_qm)
    real(dp), optional, intent(out):: stress(3,3)
    character(len=80) :: message
    real(dp)          :: e, f(3,na_qm), s(3,3)
    logical           firstTime
    save              firstTime
    data              firstTime /.true./

    integer           :: i, ia, ip, iu, n

    integer :: resonance_id

    if (present(energy)) energy = 0.0_dp
    if (present(fa))     fa     = 0.0_dp
    if (present(stress)) stress = 0.0_dp

    do resonance_id=1,num_resonances

       ! Find system index
       ip = idx( trim(resonance(resonance_id)%path)//label )

       ! Print coords for debugging
       print'(/,2a)',         'siesta_receive_forces: label = ', &
                    trim(resonance(resonance_id)%path)//trim(label)
       print*,'##################################################'

       ! Read forces from pipe
       iu = p(ip)%iuf

       read(iu,*) message

       if (message=='error') then
          read(iu,*) message
          print*, 'siesta_forces: siesta ERROR:'
          print*, trim(message)
          stop
       else if (message/='begin_forces') then
          print*, 'siesta_forces: ERROR: unexpected header:'
          print*, trim(message)
          stop
       end if

       read(iu,*) e
       do i = 1,3
          read(iu,*) s(:,i)
       end do
       read(iu,*) n
       if (n /= na_qm+numlink) then
          print*, 'siesta_forces: ERROR: na_u mismatch: na_u, n =', na_qm+numlink, n
          stop
       end if
       do ia = 1,na_qm
          read(iu,*) f(:,ia)
       end do
       do ia = 1,numlink
          read(iu,*) fa_link(resonance_id,:,ia)
       end do

       read(iu,*) message
       if (message/='end_forces') then
          print*, 'siesta_forces: ERROR: unexpected trailer:'
          print*, trim(message)
       end if

       ! Print forces for debugging
       print'(3a,f12.6)',      'siesta_receive_forces: energy (',&
                                  trim(eunit),') =', e
       print'(3a,/,(3f12.6))', 'siesta_receive_forces: stress (',&
                                  trim(sunit),') =', s
       print'(3a,/,(3f12.6))', 'siesta_receive_forces: forces (',&
                                  trim(funit),') =', f

       ! Copy results to output arguments
       if (present(energy)) energy = energy + resonance(resonance_id)%weight*e
       if (present(fa))     fa     = fa + resonance(resonance_id)%weight*f
       if (present(stress)) stress = stress + resonance(resonance_id)%weight*s

       print*,'energy= ',e, resonance(resonance_id)%weight,energy

    enddo

    firstTime=.false.

    if (num_resonances>1) then
       ! Print forces for debugging
       print'(3a,f12.6)',      'siesta_receive_forces: total QM energy (',&
            trim(eunit),') =', energy
       print'(3a,/,(3f12.6))', 'siesta_receive_forces: total QM stress (',&
            trim(sunit),') =', stress
       print'(3a,/,(3f12.6))', 'siesta_receive_forces: total QM forces (',&
            trim(funit),') =', fa
    endif

  end subroutine siesta_receive_forces

!---------------------------------------------------

recursive subroutine siesta_quit( label )
  implicit none
  character(len=*), intent(in) :: label

  integer :: ip, iuc, iuf
  character(len=20) message

  if (label == 'all') then
    ! Stop all siesta processes
    do ip = 1,np
      call siesta_quit( p(ip)%label )
    end do
  else
    ! Stop one siesta process
    ip = idx(label)      ! Find process index
    if (ip==0) then
      print*, 'siesta_quit: ERROR: unknown label: ', trim(label)
      stop
    end if
    iuc = p(ip)%iuc      ! Find cooordinates pipe unit
    write(iuc,*) 'quit'  ! Send quit signal through pipe
    call pxfflush(iuc)
    iuf = p(ip)%iuf                  ! Find forces pipe unit
    read(iuf,*) message              ! Receive response from pipe
    if (message == 'quitting') then  ! Check answer
      close(iuc,status="delete")     ! Close coordinates pipe
      close(iuf,status="delete")     ! Close forces pipe
      if (ip < np) then              ! Move last process to this slot
        p(ip)%label = p(np)%label
        p(ip)%iuc   = p(np)%iuc
        p(ip)%iuf   = p(np)%iuf
      end if
      np = np - 1                    ! Remove process
    else
      print*, 'siesta_quit: ERROR: unexpected response: ', trim(message)
      stop
    end if ! (message)
  end if ! (label)

end subroutine siesta_quit

!---------------------------------------------------

subroutine open_pipes( label )
  implicit none
  character(len=*), intent(in) :: label

  integer           :: iuc, iuf
  character(len=80) :: cpipe, fpipe

! Check that pipe does not exist already
  if (idx(label) /= 0) &
    print*, 'open_pipes: ERROR: pipes for ', trim(label), &
            ' already opened'

! Get io units for pipes
  call get_io_units( iuc, iuf )

! Open pipes
  cpipe = trim(label)//'.coords'
  fpipe = trim(label)//'.forces'
  open( unit=iuc, file=cpipe, form="formatted", &
        status="old", position="asis")
  open( unit=iuf, file=fpipe, form="formatted", &
        status="old", position="asis")

! Store data of process
  np = np + 1
  if (np > max_procs) then
    stop 'siesta_launch: ERROR: parameter max_procs too small'
  else
    p(np)%label = label
    p(np)%iuc = iuc
    p(np)%iuf = iuf
  end if

end subroutine open_pipes

!---------------------------------------------------

subroutine get_io_units( iuc, iuf)
! Finds two available I/O unit numbers
  implicit none
  integer, intent(out)  :: iuc, iuf

  integer :: i, ip
  logical :: unit_used

  iuc = 0
  iuf = 0
  do i = 10, 99
    inquire(unit=i,opened=unit_used)  ! This does not work with pipes
    do ip = 1,np
      unit_used = unit_used .or. i==p(ip)%iuc .or. i==p(ip)%iuf
    end do
    if (.not. unit_used) then
      if (iuc==0) then
        iuc = i
      else
        iuf = i
        return
      end if
    endif
  enddo
  stop 'fsiesta:get_io_units: ERROR: cannot find free I/O unit'
end subroutine get_io_units

!---------------------------------------------------

integer function idx( label )
! Finds which of the stored labels is equal to the input label
  implicit none
  character(len=*), intent(in) :: label
  integer :: i
  do i = 1,np
    if (label==p(i)%label) then
      idx = i
      return
    end if
  end do
  idx = 0
end function idx

!---------------------------------------------------

end module fsiesta
